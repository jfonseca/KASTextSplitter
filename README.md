# KASTextSplitter 1.2

This component was used to split up text into words or letters.

## Example

To run the examples you can either upload 'Example' folder to your server or run it locally by using the npm library inside the 'Example' folder.

[View Example Online](https://www.klipartstudio.com/site/KAS_TextSplitter_AnimateJS/)

[NPM](https://www.npmjs.com/get-npm) - Install NPM if you currently don't have it

* Install the package from npm

`npm install`

* Start the local server

`npm start`

## Usage

* Greensock TweenLite required

### DESCRIPTION:

* this : Scope of view
* target : Target Element Video or Audio
* name : Name that is used as ID
* text : Text you would like to split
* type : use ('letters' or 'words') or (0 or 1) this will split the text by those options
* onComplete : once the audio reach the end it will call this function

Javascript code:

```javascript
var textArr = new tfx.textFX(this, {
    target: 'textFX',
    name: 'message',
    text: inputText.value,
    type: 'words',
    onComplete: complete
});

//Optional
function complete(obj) {
    console.log('Complete element  ', obj);
}
```

HTML code:

```html
<div id='textFX'></div>
```
CSS code:

```css
.ttFX {
	position: absolute;
	color: black;
	display: block;
}
```

## Authors

* **Jose Fonseca** - _[Klip Art Studio](https://www.klipartstudio.com)_ - [GIT](https://gitlab.com/jfonseca/KASAudioVideoPlayer)

## License

MIT
