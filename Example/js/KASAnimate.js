/*!
 * KAS_Animate: 1.2
 * http://www.klipartstudio.com/
 * GIT: https://gitlab.com/jfonseca
 *
 * Copyright 2012, Klip Art Studio, LLC
 *
 * BROWSER SUPPORT: Safri 5.0.2, IE8, FF 3.6.10
 * 
 * DESCRIPTION: (This code was built in as3 then convert to javascript)
 *   target    : Div ID you would like to target
 *   x		     : animate to this x position
 *   y		     : animate to this y position
 *   width     : animate width to this point
 *   height    : animate height to this point
 *   alpha     : animate translucent
 * 	 startX	    : Start animation x position
 * 	 startY		: Start animation y position
 * 	 startAlpha : Start animation translucent position
 * 	 startWidth : Start animation width position
 * 	 startHeight: Start animation height position
 *   color     : Use hex value to define color (used for text and other items that use color)
 *   BGcolor   : Use hex value to define color (this to specify background color of items)
 *   speed     : How fast till it is done animating
 *   delay     : How long to wait before it starts animating
 *   ease      : ease into place 'backeaseOut','bounceEaseOut','circEaseOut','cubicEaseOut','elasticEaseOut','expoEaseOut','linearEaseOut','quadEaseOut','quartEaseOut'
 *   onComplete: Call a function once it is complete
 *
 * 	 CSS3 Params Option (CSS3:true) must be set:
 *   		scale   : 1.0 is 100 percent of current item
 *   		rotate  : Rotation of item
 *     	 startScale : Start animation Scale
 *
 *
 *  
 * USAGE:
 *		var items = new kas.Animate();
 * 		items.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'finishedFunction', alpha:0, delay:.1});
 *
 * CSS3 Option:
 *		var itemsCSS3 = new kas.Animate();
 * 		itemsCSS3.Tween(this, {target:'divID',y:randRange(10, 500), x:randRange(10, 500), duration:1, ease:'linearEaseOut', onComplete:'finishedFunction', alpha:0, delay:.1, CSS3:true, scale:scale, rotate:rotation});
 *
 * Timer Option:
 *		var timer = new kas.Animate();
 * 		timer.Tween(this, {duration:5, timer:contiousFunctionCall, onComplete:finishedFunction});
 *
 * AUTHOR: Jose Fonseca, josef@klipartstudio.com
 * DATE: 11/11/2011
 * 
 * 
 */
var kas = {};

kas.Animate = function() {
  this.init = false;
  this.timer = 0;
  this.fromToVal = {};
  this.requestId = 0;
  this.animationStartTime = 0;
  this.centerX = 500 / 2;
  this.centerY = 500 / 2;
  this.vx = 0;
  this.vy = 0;
  this.xPos = 0;
  this.yPos = 0;
  this.delay;
  this.alpha = 0;
  this.sAlpha = 0;
  this.masterList = [];
  this.count = 0;
  this.renderQ = [];
  this.cached = [];
  return this;
};
kas.Animate.prototype.findPosX = function(obj) {
  var curleft = 0;
  if (obj.offsetParent)
    while (1) {
      curleft += obj.offsetLeft;
      if (!obj.offsetParent) break;
      obj = obj.offsetParent;
    }
  else if (obj.x) curleft += obj.x;
  return curleft;
};

kas.Animate.prototype.findPosY = function(obj) {
  var curtop = 0;
  if (obj.offsetParent)
    while (1) {
      curtop += obj.offsetTop;
      if (!obj.offsetParent) break;
      obj = obj.offsetParent;
    }
  else if (obj.y) curtop += obj.y;
  return curtop;
};
kas.Animate.prototype.creatediv = function(id, html, width, height, left, top) {
  var newdiv = document.createElement("div");
  newdiv.setAttribute("id", id);
  if (width) {
    newdiv.style.width = width;
  }
  if (height) {
    newdiv.style.height = height;
  }
  if (left || top || (left && top)) {
    newdiv.style.position = "absolute";
    if (left) {
      newdiv.style.left = left;
    }
    if (top) {
      newdiv.style.top = top;
    }
  }
  newdiv.style.background = "#00C";
  newdiv.style.border = "0px solid #000";
  newdiv.style.display = "none";
  if (html) {
    newdiv.innerHTML = html;
  } else {
    newdiv.innerHTML = "";
  }

  return document.body.appendChild(newdiv);
};
kas.Animate.prototype.Tween = function(scope, p_parameters) {
  //clearTimeout(this.timer);
  //this.p_obj = p_parameters;

  var targetItem;
  p_parameters["name"] = p_parameters["name"]
    ? p_parameters["name"]
    : "KAS" + this.masterList.length; //p_parameters.target

  if (p_parameters.target != null) {
    if (typeof p_parameters.target == "object") {
      targetItem = p_parameters.target;
      //utils.trace(document)
      utils.trace(
        "----------  OBJECT  -----------" + p_parameters.target.attributes
      );
    } else {
      targetItem = document.getElementById(p_parameters.target);
    }
  } else {
    console.log(
      p_parameters.name + "item =========   " + typeof p_parameters.target
    );
    targetItem = this.creatediv(p_parameters.name, null, 0, 0, 0, 0);
    //targetItem = document.createElement('DIV');
    //targetItem.setAttribute('id', p_parameters.name);
  }

  // console.log(targetItem + " targetItem ");
  //this.cssStyle = this.p_obj.cssStyle.replace(/\W/g,'');

  // 	for (var prop in p_parameters) {
  //       console.log(prop + " 00 = 00 " + p_parameters[prop]);
  //    }

  //utils.trace('----------  HEIGHT  -----------' + targetItem.offsetHeight);
  //utils.trace('----------  WIDTH  -----------' + targetItem.style.width);
  p_parameters["id"] = targetItem;
  p_parameters["item"] = targetItem;
  p_parameters["scope"] = scope;
  p_parameters["sRotate"] = 0;
  p_parameters["sScale"] = 1;
  p_parameters["ease"] = p_parameters.ease
    ? p_parameters.ease
    : "linearEaseOut";
  p_parameters["sScrollTop"] =
    navigator.appName == "Microsoft Internet Explorer"
      ? document.documentElement.scrollTop
      : window.pageYOffset;
  p_parameters["sScrollLeft"] =
    navigator.appName == "Microsoft Internet Explorer"
      ? document.documentElement.scrollLeft
      : window.pageXOffset;

  p_parameters["sWidth"] = targetItem.offsetWidth;
  p_parameters["sHeight"] = targetItem.offsetHeight;
  p_parameters["xPos"] = parseInt(targetItem.offsetLeft);
  p_parameters["yPos"] = parseInt(targetItem.offsetTop); //this.findPosY(targetItem);

  utils.trace(
    "----------  parseInt(targetItem.offsetLeft)  -----------" +
      parseInt(targetItem.offsetLeft)
  );

  /*
	 p_parameters['xPos'] = this.findPosX(targetItem);
	 p_parameters['yPos'] = this.findPosY(targetItem);
*/

  //utils.trace('this.p_obj.y >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '  + typeof(this.p_obj.y));
  //utils.trace('this.p_obj.y >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    '  + typeof(p_parameters['y']));
  p_parameters["vx"] = 0;
  p_parameters["vy"] = 0;
  p_parameters["va"] = 0;
  p_parameters["vs"] = 1;

  p_parameters["vst"] = 0;

  p_parameters["complete"] = false;
  p_parameters["webId"] = this.getTransformProperty(targetItem);

  var dummycolor = this.getStyle(targetItem, "backgroundColor");
  //console.log('<<<<<<dummycolor2   ' +dummycolor);
  var dummycolor2 = utils.colorToRGBObj(
    utils.rgbToHex(this.getStyle(targetItem, "color"))
  );

  var initColor = utils.colorToRGBObj(
    utils.rgbToHex(this.getStyle(targetItem, "color"))
  );
  var finColor = p_parameters.color
    ? utils.colorToRGBObj(p_parameters.color)
    : initColor;

  var initBGColor = utils.colorToRGBObj(
    utils.rgbToHex(this.getStyle(targetItem, "backgroundColor"))
  );
  //console.log('<<<<<<dummycolor2   ' +dummycolor2.rgb);
  var finBGColor = p_parameters.BGcolor
    ? utils.colorToRGBObj(p_parameters.BGcolor)
    : initBGColor;

  p_parameters["sColor"] = initColor;
  p_parameters["eColor"] = finColor;
  p_parameters["sBGColor"] = initBGColor;
  p_parameters["eBGColor"] = finBGColor;

  /*
	 for (var prop in initColor) {
      utils.trace(prop + "color = " + initColor[prop]);
   }
*/

  //  console.log('<<<<<<initColor   ' +typeof(initColor.rgb));
  //  console.log('<<<<<<finColor   ' +finColor.rgb);
  //  console.log('<<<<<<initBGColor   ' +initBGColor.rgb);
  //  console.log('<<<<<<initColor   ' +finBGColor.rgb);
  //  console.log('<<<<<<initColor   ' +finBGColor.rgb);

  p_parameters["startTimer"] = 0;
  p_parameters["mDuration"] = p_parameters.duration * 1000;
  p_parameters["negNumber"] = false;
  p_parameters["mSecond"] = 0;
  p_parameters["delayTime"] = 0;
  p_parameters["scale"] = p_parameters["scale"] ? p_parameters["scale"] : 1;
  p_parameters["delay"] = p_parameters["delay"] ? p_parameters["delay"] : 0;
  p_parameters["right"] = p_parameters["right"] ? p_parameters["right"] : false;

  p_parameters["sAlpha"] = targetItem.style.opacity
    ? parseFloat(targetItem.style.opacity) * 100
    : 0;
  if (isNaN(parseFloat(targetItem.style.opacity))) {
    p_parameters["sAlpha"] = 100;
  }

  if (!this.masterList[p_parameters.name]) {
    this.masterList[p_parameters.name] = p_parameters;

    //utils.trace('----------  NEW  -----------');
  } else {
    var updateItem = this.masterList[p_parameters.name];

    //utils.trace('----------  p_parameters.xPos  -----------' + p_parameters.xPos);
    //utils.trace('----------  p_parameters.yPos  -----------' + p_parameters.yPos);
    //utils.trace('----------  p_parameters.sAlpha  -----------' + p_parameters.sAlpha);

    p_parameters.xPos = updateItem.ox;
    p_parameters.yPos = updateItem.oy;
    p_parameters.sAlpha = updateItem.oa;
    p_parameters.sWidth = updateItem.ow;
    p_parameters.sHeight = updateItem.oh;
    p_parameters.sRotate = updateItem.or;

    p_parameters.vx = updateItem.ox;
    p_parameters.vy = updateItem.oy;
    p_parameters.va = updateItem.oa;
    p_parameters.vr = updateItem.oa;

    p_parameters.ox = updateItem.ox;
    p_parameters.oy = updateItem.oy;
    p_parameters.oa = updateItem.oa;
    p_parameters.or = updateItem.or;

    //utils.trace('----------  updateItem.ox  -----------' + updateItem.ox);
    //utils.trace('----------  updateItem.oy  -----------' + updateItem.oy);
    //utils.trace('----------  updateItem.oa  -----------' + updateItem.oa);

    this.masterList[p_parameters.name] = p_parameters;

    utils.trace("----------  UPDATE  -----------");
  }

  this.startInit(p_parameters.name);
};

/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.animate = function(selector) {
  var prams = selector;
  var item = this.masterList[selector];

  //utils.trace('item.duration  ' + item.mDuration);
  //Duration Check
  if (item.mDuration) {
    item.duration = item.mDuration;
  } else if (item.duration) {
    item.duration = item.duration;
  } else {
    item.duration = 0;
  }

  //Check for negative value
  if (item.end < 0) {
    item.negNumber = true;
  }

  //check if value in the mid
  item.mSecond = item.mDuration;
  if (!prams.scale) {
    //utils.trace('item.mSecond  ' + item.mDuration);
  } else {
    //this.mSecond =  this.duration*(Math.abs((finishVal-startVal)/(scaleVal[1]-scaleVal[0])));
  }

  //check type of animation cos, sin or Linear
  /*
	if(typeof(prams.ease)=='string'){
		this.tranType =  prams.ease.charAt(0).toLowerCase();
	} else if(this.tranType) {
		this.tranType = this.tranType;
	} else {
		this.tranType = '';
	}
*/

  //The time the game started
  var getTime = new Date().getTime();

  item.startTimer = getTime + item.delay * 1000;

  this.init = true;
  this.initTimer = true;
  //this.update();
  var self = this;
  if (self.timer == 0) {
    (function drawFrame() {
      self.timer = window.requestAnimationFrame(drawFrame);

      self.update();
    })();
  }
};
kas.Animate.prototype.getStyle = function(elem, name) {
  if (document.defaultView && document.defaultView.getComputedStyle) {
    name = name.replace(/([A-Z])/g, "-$1");
    name = name.toLowerCase();
    s = document.defaultView.getComputedStyle(elem, "");
    return s && s.getPropertyValue(name);
  } else if (elem.currentStyle) {
    if (/backgroundcolor/i.test(name)) {
      return (function(el) {
        // get a rgb based color on IE
        var oRG = document.body.createTextRange();
        oRG.moveToElementText(el);
        var iClr = oRG.queryCommandValue("BackColor");
        return (
          "rgb(" +
          (iClr & 0xff) +
          "," +
          ((iClr & 0xff00) >> 8) +
          "," +
          ((iClr & 0xff0000) >> 16) +
          ")"
        );
      })(elem);
    }

    return elem.currentStyle[name];
  } else if (elem.style[name]) {
    return elem.style[name];
  } else {
    return null;
  }
};
/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.icolor = function(item, timePassed) {
  var cR = item.eColor.r - item.sColor.r;
  var cG = item.eColor.g - item.sColor.g;
  var cB = item.eColor.b - item.sColor.b;

  tR = this[item.ease](timePassed, item.sColor.r, cR, item.mSecond);
  tG = this[item.ease](timePassed, item.sColor.g, cG, item.mSecond);
  tB = this[item.ease](timePassed, item.sColor.b, cB, item.mSecond);

  item.vc =
    "rgb(" + Math.floor(tR) + "," + Math.floor(tG) + "," + Math.floor(tB) + ")";

  var cBGR = item.eBGColor.r - item.sBGColor.r;
  var cBGG = item.eBGColor.g - item.sBGColor.g;
  var cBGB = item.eBGColor.b - item.sBGColor.b;

  tBGR = this[item.ease](timePassed, item.sBGColor.r, cBGR, item.mSecond);
  tBGG = this[item.ease](timePassed, item.sBGColor.g, cBGG, item.mSecond);
  tBGB = this[item.ease](timePassed, item.sBGColor.b, cBGB, item.mSecond);

  item.vbgc =
    "rgb(" +
    Math.floor(tBGR) +
    "," +
    Math.floor(tBGG) +
    "," +
    Math.floor(tBGB) +
    ")";
};

/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.update = function() {
  var self = this;
  var arrayLength = 0;
  for (var prop in this.masterList) {
    var item = this.masterList[prop];
    var timePassed = new Date().getTime() - item.startTimer;
    item.timePassed += timePassed;
    utils.trace(new Date().getTime() + "  item.timePassed  " + item.startTimer);

    /*
		for (var prop2 in this.masterList[prop]) {
    	utils.trace(prop2 + " = " + this.masterList[prop][prop2]);
 		}
*/

    var cX = item.x - item.xPos;
    var cY = item.y - item.yPos;
    var cA = item.alpha - item.sAlpha;

    var cH = item.height - item.sHeight;
    var cW = item.width - item.sWidth;
    var cR = item.rotate - item.sRotate;
    var cS = item.scale - item.sScale;

    var cST = item.scrollTop - item.sScrollTop;
    var cSL = item.scrollLeft - item.sScrollLeft;

    /*
		console.log(infoColor.r + '<<<<<COLOR  ' + item.color);
		console.log(item.sColor + '<<<<<<<<START COLOR  ' + item.sColor);
		console.log(this.getStyle(item.item,"backgroundColor") + '<<<<<<<<DIV COLOR COLOR  ');
*/

    if (timePassed > 0) {
      item.vx = this[item.ease](timePassed, item.xPos, cX, item.mSecond);
      item.vy = this[item.ease](timePassed, item.yPos, cY, item.mSecond);
      item.va = this[item.ease](timePassed, item.sAlpha, cA, item.mSecond);

      item.vw = this[item.ease](timePassed, item.sWidth, cW, item.mSecond);
      item.vh = this[item.ease](timePassed, item.sHeight, cH, item.mSecond);
      item.vr = this[item.ease](timePassed, item.sRotate, cR, item.mSecond);
      item.vs = this[item.ease](timePassed, item.sScale, cS, item.mSecond);
      item.vst = this[item.ease](
        timePassed,
        item.sScrollTop,
        cST,
        item.mSecond
      );
      item.vsl = this[item.ease](
        timePassed,
        item.sScrollLeft,
        cSL,
        item.mSecond
      );

      this.icolor(item, timePassed);

      if (item.timer) {
        item.timer(timePassed);
      }

      if (item.CSS3) {
        this.stageUpdateCSS(item);
        //utils.trace();
      } else {
        this.stageUpdate(item);
      }
      if (timePassed > item.mSecond) {
        item.vx = item.x;
        item.vy = item.y;
        item.va = item.alpha;

        item.vw = item.width;
        item.vh = item.height;
        item.vr = item.rotate;
        item.vs = item.scale;

        item.vst = item.scrollTop;

        item.complete = true;

        if (item.CSS3) {
          this.stageUpdateCSS(item);
        } else {
          this.stageUpdate(item);
        }

        delete this.masterList[prop];
        if (item.onComplete) {
          item.onComplete(213);
        }
      }
    }
    arrayLength++;
  }
  if (self.timer && arrayLength == 0) {
    window.cancelRequestAnimationFrame(self.timer);
    self.timer = 0;
  }
};
/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.transform = function(clip) {
  var el = clip;
  var st = window.getComputedStyle(el, null);
  var tr =
    st.getPropertyValue("-webkit-transform") ||
    st.getPropertyValue("-moz-transform") ||
    st.getPropertyValue("-ms-transform") ||
    st.getPropertyValue("-o-transform") ||
    st.getPropertyValue("transform") ||
    "fail...";

  /*
		 	for (var prop2 in tr) {
      	utils.trace(tr + " = " + tr[prop2]);
   		}
*/
  // With rotate(30deg)...
  // matrix(0.866025, 0.5, -0.5, 0.866025, 0px, 0px)
  //utils.trace('Matrix: ' + tr);
  if (tr == "none") {
    return;
  }
  // rotation matrix - http://en.wikipedia.org/wiki/Rotation_matrix

  var values = tr
    .split("(")[1]
    .split(")")[0]
    .split(",");
  var a = values[0];
  var b = values[1];
  var c = values[2];
  var d = values[3];
  var e = values[4];
  var f = values[5];
  /*
	
	for (var prop2 in values) {
  	utils.trace(tr + " = " + values[prop2]);
		}
*/

  utils.trace("a: " + a);
  utils.trace("b: " + b);
  utils.trace("c: " + c);
  utils.trace("d: " + d);
  utils.trace("e: " + e);
  utils.trace("f: " + f);
  var scale = Math.sqrt(a * a + b * b);
  utils.trace("scale: " + scale);
  // arc sin, convert from radians to degrees, round
  // DO NOT USE: see update below
  var sin = b / scale;
  var angle = Math.round(Math.atan2(b, a) * (180 / Math.PI));
  var angle2 = Math.round(Math.atan2(b, a) * (180 / Math.PI));
  // works!
  utils.trace("Rotate: " + angle + "deg");
  utils.trace("Rotate2: " + angle2 + "deg");

  values[6] = scale;
  values[7] = angle;
  return values;
};
kas.Animate.prototype.getTransformProperty = function(element) {
  // Note that in some versions of IE9 it is critical that
  // msTransform appear in this list before MozTransform
  var properties = [
    "transform",
    "WebkitTransform",
    "msTransform",
    "MozTransform",
    "OTransform"
  ];
  var p;
  while ((p = properties.shift())) {
    if (typeof element.style[p] != "undefined") {
      return p;
    }
  }
  return false;
};

kas.Animate.prototype.degreesToRadians = function(num) {
  return num * Math.PI / 180;
};

/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.stageUpdateCSS = function(clip) {
  var xid, yid, rotateid;
  if (isFinite(clip.vx) && clip.vx - clip.ox != NaN) {
    if (clip.vx < 0 && !clip.negNumber) {
      //clip.vx = 0;
    }
    clip.ox = Math.floor(clip.vx);
    clip.vx = Math.floor(clip.vx);
  }
  if (isFinite(clip.vy) && clip.vx - clip.oy != NaN) {
    if (clip.vy < 0 && !clip.negNumber) {
      //clip.vy = 0;
    }
    clip.oy = Math.floor(clip.vy);
    clip.vy = Math.floor(clip.vy);
  }
  if (isFinite(clip.va) && clip.va - clip.oa != NaN) {
    if (clip.va < 0 && !clip.negNumber) {
      clip.va = 0;
    }
    clip.oa = clip.va;
    clip.item.style.opacity = clip.va / 100;
    clip.item.style.filter = "alpha(opacity=" + clip.va + ")";
  }
  if (clip.BGcolor) {
    clip.obgc = clip.vc;
    clip.item.style.backgroundColor = clip.vbgc;
  }
  if (clip.color) {
    clip.oc = clip.vc;
    clip.item.style.color = clip.vc;
  }
  /*
	if(isFinite(clip.vw) && (clip.vw - clip.ow) != NaN){
		if (clip.vw < 0){
			clip.vw = 0;
		}
		clip.ow = Math.floor(clip.vw);
		clip.item.style.width = clip.vw+'px';
	}
	if(isFinite(clip.vh) && (clip.vh - clip.oh) != NaN){
		if (clip.vh < 0){
			clip.vh = 0;
		}
		clip.oh = Math.floor(clip.vh);
		clip.item.style.height = clip.vh+'px';
	}
*/
  if (isFinite(clip.vr) && clip.vr - clip.or != NaN) {
    if (clip.vr < 0) {
      clip.vr = 0;
    }
    clip.or = clip.vr; //-webkit-transform:rotate(120deg);
  }
  if (isFinite(clip.vs) && clip.vs - clip.os != NaN) {
    if (clip.vs < 0) {
      clip.vs = 0;
    }
    clip.os = clip.vs; //-webkit-transform:rotate(120deg);
  }
  if (isFinite(clip.vst) && clip.vst - clip.ost != NaN) {
    if (clip.vst < 0) {
      clip.vst = 0;
    }
    clip.ost = Math.floor(clip.vst);
    window.scrollTo(clip.vsl, clip.vst);
  }
  if (isFinite(clip.vsl) && clip.vsl - clip.osl != NaN) {
    if (clip.vsl < 0) {
      clip.vsl = 0;
    }
    clip.osl = Math.floor(clip.vsl);
    window.scrollTo(clip.vsl, clip.vst);
  }
  var newRot = this.degreesToRadians(clip.vr);
  newRot = -newRot;

  if (isNaN(newRot)) {
    newRot = 0;
    newRot = 0;
    utils.trace("  NaNNaNNaNNaNNaNNaNNaNNaNNaN ");
  }

  var addPX = "";

  if (clip.webId == "MozTransform") {
    addPX = "px";
  }
  var matrixItem =
    "matrix(" +
    Math.cos(newRot) * clip.vs +
    ", " +
    -Math.sin(newRot) * clip.vs +
    "," +
    Math.sin(newRot) * clip.vs +
    "," +
    Math.cos(newRot) * clip.vs +
    "," +
    clip.vx +
    addPX +
    "," +
    clip.vy +
    addPX +
    ")";
  utils.trace(
    newRot +
      "<<<<<<  newRot   -   Math.cos(newRot)  >>>>>>>>>>>>>" +
      Math.cos(newRot)
  );

  utils.trace(clip.x + " <<<<<<X clip.vx " + clip.vx);
  utils.trace("matrixItem  >>  " + matrixItem);

  clip.item.style.left = Math.floor(0) + "px";
  clip.item.style.top = Math.floor(0) + "px";
  clip.item.style[clip.webId] = matrixItem;
  clip.item.style[clip.webId + "Origin"] = "50% 50%";
};

/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.stageUpdate = function(clip) {
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>START>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );
  utils.trace("NAME        = " + clip.dummy);
  utils.trace("TIME PASSED = " + clip.timePassed);
  utils.trace("vy   			 = " + Math.floor(clip.vy));
  utils.trace("yPos  			 = " + clip.yPos);
  utils.trace("clipy  		 = " + clip.y);
  utils.trace("xPos  			 = " + clip.xPos);
  utils.trace("vx   			 = " + Math.floor(clip.vx));
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>END>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );

  if (isFinite(clip.vx) && clip.vx - clip.ox != NaN) {
    clip.ox = Math.floor(clip.vx);
    if (clip.right) {
      clip.item.style.right = Math.floor(clip.vx) + "px";
    } else {
      clip.item.style.left = Math.floor(clip.vx) + "px";
    }
  }

  if (clip.BGcolor) {
    clip.obgc = clip.vc;
    clip.item.style.backgroundColor = clip.vbgc;
  }
  if (clip.color) {
    clip.oc = clip.vc;
    clip.item.style.color = clip.vc;
  }
  if (isFinite(clip.vy) && clip.vy - clip.oy != NaN) {
    clip.oy = Math.floor(clip.vy);
    clip.item.style.top = Math.floor(clip.vy) + "px";
  }
  if (isFinite(clip.va) && clip.va - clip.oa != NaN) {
    if (clip.va < 0 && !clip.negNumber) {
      clip.va = 0;
    }
    clip.oa = clip.va;
    clip.item.style.opacity = clip.va / 100;
    clip.item.style.filter = "alpha(opacity=" + clip.va + ")";
  }
  if (isFinite(clip.vw) && clip.vw - clip.ow != NaN) {
    if (clip.vw < 0) {
      clip.vw = 0;
    }
    clip.ow = Math.floor(clip.vw * clip.vs);
    clip.item.style.width = clip.vw * clip.vs + "px";
  }
  if (isFinite(clip.vh) && clip.vh - clip.oh != NaN) {
    if (clip.vh < 0) {
      clip.vh = 0;
    }
    clip.oh = Math.floor(clip.vh);
    clip.item.style.height = clip.vh * clip.vs + "px";
  }
  if (isFinite(clip.vst) && clip.vst - clip.ost != NaN) {
    if (clip.vst < 0) {
      clip.vst = 0;
    }
    clip.ost = Math.floor(clip.vst);
    window.scrollTo(clip.vsl, clip.vst);
  }
  //this.transform(clip);
};
/**
 * This is the default comment. If a page is not
 * specified, then this will run.
 *
 * @return void
 **/
kas.Animate.prototype.startInit = function(selector) {
  var clip = this.masterList[selector];
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>START>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );
  utils.trace("startX      = " + clip.startX);
  utils.trace("startY      = " + clip.startY);
  utils.trace("startAlpha  = " + String(clip.startAlpha));
  utils.trace("startScale  = " + clip.startScale);
  utils.trace("startWidth  = " + clip.startWidth);
  utils.trace("startHeight = " + clip.startHeight);
  utils.trace("vx   			 = " + clip.vx);
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );

  /* clip.item.style.overflow = clip.hide ? 'hidden' : 'visible'; */
  if (clip.hide) {
    clip.item.style.overflow = "none";
  }

  for (var prop2 in this.masterList[selector]) {
    utils.trace(prop2 + " ======= " + this.masterList[selector][prop2]);
  }

  if (clip.startColor) {
    clip.sColor = utils.colorToRGBObj(clip.startColor);

    clip.item.style.backgroundColor = clip.sColor.rgb;
  }
  if (clip.startX) {
    clip.xPos = clip.startX;
    if (clip.right) {
      clip.item.style.right = clip.startX + "px";
    } else {
      utils.trace(
        " ====================================================  LEFT"
      );
      clip.item.style.left = clip.startX + "px";
    }
  }
  if (clip.startY) {
    clip.yPos = clip.startY;
    clip.item.style.top = Math.floor(clip.startY) + "px";
  }
  if (clip.CSS3) {
    var matrix = this.transform(clip.item);

    if (typeof matrix == "object") {
      var xX = parseFloat(clip.item.style.left);
      var yY = parseFloat(clip.item.style.top);
      if (xX == 0) {
        xX = matrix[4];
      }
      if (yY == 0) {
        yY = matrix[5];
      }
      clip.xPos = parseFloat(xX);
      clip.yPos = parseFloat(yY);
      clip.sRotate = parseFloat(matrix[7]);
      clip.sScale = parseFloat(matrix[6]);
      clip.item.style.left = Math.floor(0) + "px";
      clip.item.style.top = Math.floor(0) + "px";
      for (var prop2 in matrix) {
        utils.call(prop2 + " ======= " + matrix[prop2]);
      }
      utils.call(" clip.item.style.left ======= " + clip.item.style.left);
      utils.call(" clip.item.style.top ======= " + clip.item.style.top);
      var newRot;
      if (clip.startRotate) {
        newRot = this.degreesToRadians(clip.startRotate);
      }

      newRot = -newRot;
      if (isNaN(newRot)) {
        newRot = 0;
        newRot = 0;
      }

      var addPX = "";

      if (clip.webId == "MozTransform") {
        addPX = "px";
      }
      var matrixItem =
        "matrix(" +
        Math.cos(newRot) * scaleItem +
        ", " +
        -Math.sin(newRot) * scaleItem +
        "," +
        Math.sin(newRot) * scaleItem +
        "," +
        Math.cos(newRot) * scaleItem +
        "," +
        clip.xPos +
        addPX +
        "," +
        clip.yPos +
        addPX +
        ")";

      clip.item.style[clip.webId] = matrixItem;
      clip.item.style[clip.webId + "Origin"] = "50% 50%";
    }

    //clip.item.style[clip.webId]
  }
  if (String(clip.startAlpha) != "undefined") {
    clip.sAlpha = clip.startAlpha;
    clip.item.style.opacity = clip.startAlpha / 100;
    clip.item.style.filter = "alpha(opacity=" + clip.startAlpha + ")";

    utils.trace(
      " ====================================================  clip.item.style.opacity  " +
        clip.item.style.opacity
    );
  }
  var scaleItem = 1;
  if (clip.startScale) {
    clip.sScale = clip.startScale;
    scaleItem = clip.startScale;
  }
  if (clip.startWidth) {
    clip.item.style.width = clip.startWidth * scaleItem + "px";
  }
  if (clip.startHeight) {
    clip.item.style.height = clip.startHeight * scaleItem + "px";
  }
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>END>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" +
      selector
  );
  utils.trace(
    ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
  );

  this.animate(selector);

  //this.transform(clip);
};
// BACK
kas.Animate.prototype.backeaseIn = function(t, b, c, d, s) {
  if (s == undefined) s = 1.70158;
  return c * (t /= d) * t * ((s + 1) * t - s) + b;
};
kas.Animate.prototype.backeaseOut = function(t, b, c, d, s) {
  if (s == undefined) s = 1.70158;
  return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
};
kas.Animate.prototype.backeaseInOut = function(t, b, c, d, s) {
  if (s == undefined) s = 1.70158;
  if ((t /= d / 2) < 1)
    return c / 2 * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
  return c / 2 * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
};

// BOUNCE
kas.Animate.prototype.bounceEaseOut = function(t, b, c, d) {
  if ((t /= d) < 1 / 2.75) {
    return c * (7.5625 * t * t) + b;
  } else if (t < 2 / 2.75) {
    return c * (7.5625 * (t -= 1.5 / 2.75) * t + 0.75) + b;
  } else if (t < 2.5 / 2.75) {
    return c * (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375) + b;
  } else {
    return c * (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375) + b;
  }
};
kas.Animate.prototype.bounceEaseIn = function(t, b, c, d) {
  return c - this.bounceEaseOut(d - t, 0, c, d) + b;
};
kas.Animate.prototype.bounceEaseInOut = function(t, b, c, d) {
  if (t < d / 2) return this.bounceEaseIn(t * 2, 0, c, d) * 0.5 + b;
  else return this.bounceEaseOut(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b;
};

// Circle
kas.Animate.prototype.circEaseIn = function(t, b, c, d) {
  return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
};
kas.Animate.prototype.circEaseOut = function(t, b, c, d) {
  return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
};
kas.Animate.prototype.circEaseInOut = function(t, b, c, d) {
  if ((t /= d / 2) < 1) return -c / 2 * (Math.sqrt(1 - t * t) - 1) + b;
  return c / 2 * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
};

// Cubic
kas.Animate.prototype.cubicEaseIn = function(t, b, c, d) {
  return c * (t /= d) * t * t + b;
};
kas.Animate.prototype.cubicEaseOut = function(t, b, c, d) {
  return c * ((t = t / d - 1) * t * t + 1) + b;
};
kas.Animate.prototype.cubicEaseInOut = function(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
  return c / 2 * ((t -= 2) * t * t + 2) + b;
};

// Elastic
kas.Animate.prototype.elasticEaseIn = function(t, b, c, d, a, p) {
  if (t == 0) return b;
  if ((t /= d) == 1) return b + c;
  if (!p) p = d * 0.3;
  if (!a || a < Math.abs(c)) {
    a = c;
    var s = p / 4;
  } else var s = p / (2 * Math.PI) * Math.asin(c / a);
  return (
    -(
      a *
      Math.pow(2, 10 * (t -= 1)) *
      Math.sin((t * d - s) * (2 * Math.PI) / p)
    ) + b
  );
};
kas.Animate.prototype.elasticEaseOut = function(t, b, c, d, a, p) {
  if (t == 0) return b;
  if ((t /= d) == 1) return b + c;
  if (!p) p = d * 0.3;
  if (!a || a < Math.abs(c)) {
    a = c;
    var s = p / 4;
  } else var s = p / (2 * Math.PI) * Math.asin(c / a);
  return (
    a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b
  );
};
kas.Animate.prototype.elasticEaseInOut = function(t, b, c, d, a, p) {
  if (t == 0) return b;
  if ((t /= d / 2) == 2) return b + c;
  if (!p) p = d * (0.3 * 1.5);
  if (!a || a < Math.abs(c)) {
    a = c;
    var s = p / 4;
  } else var s = p / (2 * Math.PI) * Math.asin(c / a);
  if (t < 1)
    return (
      -0.5 *
        (a *
          Math.pow(2, 10 * (t -= 1)) *
          Math.sin((t * d - s) * (2 * Math.PI) / p)) +
      b
    );
  return (
    a *
      Math.pow(2, -10 * (t -= 1)) *
      Math.sin((t * d - s) * (2 * Math.PI) / p) *
      0.5 +
    c +
    b
  );
};

// Expo

kas.Animate.prototype.expoEaseIn = function(t, b, c, d) {
  return t == 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
};
kas.Animate.prototype.expoEaseOut = function(t, b, c, d) {
  return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b;
};
kas.Animate.prototype.expoEaseInOut = function(t, b, c, d) {
  if (t == 0) return b;
  if (t == d) return b + c;
  if ((t /= d / 2) < 1) return c / 2 * Math.pow(2, 10 * (t - 1)) + b;
  return c / 2 * (-Math.pow(2, -10 * --t) + 2) + b;
};

// Linear

kas.Animate.prototype.linearEaseNone = function(t, b, c, d) {
  return c * t / d + b;
};
kas.Animate.prototype.linearEaseIn = function(t, b, c, d) {
  return c * t / d + b;
};
kas.Animate.prototype.linearEaseOut = function(t, b, c, d) {
  return c * t / d + b;
};
kas.Animate.prototype.linearEaseInOut = function(t, b, c, d) {
  return c * t / d + b;
};

// Quad

kas.Animate.prototype.quadEaseIn = function(t, b, c, d) {
  return c * (t /= d) * t + b;
};
kas.Animate.prototype.quadEaseOut = function(t, b, c, d) {
  return -c * (t /= d) * (t - 2) + b;
};
kas.Animate.prototype.quadEaseInOut = function(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t + b;
  return -c / 2 * (--t * (t - 2) - 1) + b;
};

// Quarter

kas.Animate.prototype.quartEaseIn = function(t, b, c, d) {
  return c * (t /= d) * t * t * t + b;
};
kas.Animate.prototype.quartEaseOut = function(t, b, c, d) {
  return -c * ((t = t / d - 1) * t * t * t - 1) + b;
};
kas.Animate.prototype.quartEaseInOut = function(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
  return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
};

// Quint

kas.Animate.prototype.quintEaseIn = function(t, b, c, d) {
  return c * (t /= d) * t * t * t * t + b;
};
kas.Animate.prototype.quintEaseOut = function(t, b, c, d) {
  return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
};
kas.Animate.prototype.quintEaseInOut = function(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
  return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
};

// Sine

kas.Animate.prototype.sineEaseIn = function(t, b, c, d) {
  return -c * Math.cos(t / d * (Math.PI / 2)) + c + b;
};
kas.Animate.prototype.sineEaseOut = function(t, b, c, d) {
  return c * Math.sin(t / d * (Math.PI / 2)) + b;
};
kas.Animate.prototype.sineEaseInOut = function(t, b, c, d) {
  return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
};
